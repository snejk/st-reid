from __future__ import print_function, division

import scipy.io
import torch
import numpy as np

from pathlib import Path


def evaluate(qf, ql, qc, qfr, gf, gl, gc, gfr, distribution):
    query = qf
    score = np.dot(gf, query)

    interval = 100
    alpha = 5
    score_st = np.zeros(len(gc))
    for i in range(len(gc)):
        time_diff = int((qfr - gfr[i]) / interval)
        if time_diff > 0:
            pr = distribution[qc-1][gc[i]-1][time_diff]
        else:
            pr = distribution[gc[i]-1][qc-1][np.abs(time_diff)]

        score_st[i] = pr

    score  = 1 / (1 + np.exp(-alpha * score)) * 1 / (1 + 2 * np.exp(-alpha * score_st))
    index = np.argsort(-score)  # from large to small
    query_index = np.argwhere(gl == ql)
    camera_index = np.argwhere(gc == qc)
    good_index = np.setdiff1d(query_index, camera_index, assume_unique=True)
    junk_index1 = np.argwhere(gl == -1)
    junk_index2 = np.intersect1d(query_index, camera_index)
    junk_index = np.append(junk_index2, junk_index1)
    CMC_tmp = compute_mAP(index, good_index, junk_index)
    return CMC_tmp


def compute_mAP(index, good_index, junk_index):
    ap = 0
    cmc = torch.IntTensor(len(index)).zero_()
    if good_index.size == 0:
        cmc[0] = -1
        return ap, cmc

    # remove junk_index
    mask = np.in1d(index, junk_index, invert=True)
    index = index[mask]

    # find good_index
    ngood = len(good_index)
    mask = np.in1d(index, good_index)
    rows_good = np.argwhere(mask == True)
    rows_good = rows_good.flatten()

    cmc[rows_good[0]:] = 1
    for i in range(ngood):
        d_recall = 1.0 / ngood
        precision = (i + 1) * 1.0 / (rows_good[i] + 1)
        if rows_good[i] != 0:
            old_precision = i * 1.0 / rows_good[i]
        else:
            old_precision = 1.0
        ap = ap + d_recall * (old_precision + precision) / 2

    return ap, cmc


def main():
    model_name = "market1501"
    working_dir = Path(".").absolute()

    result = scipy.io.loadmat(f"{working_dir}/model/{model_name}/visual_features.mat")
    query_feature = result['query_f']
    query_cam = result['query_cam'][0]
    query_label = result['query_label'][0]
    query_frames = result['query_frames'][0]

    gallery_feature = result['gallery_f']
    gallery_cam = result['gallery_cam'][0]
    gallery_label = result['gallery_label'][0]
    gallery_frames = result['gallery_frames'][0]

    query_feature = query_feature.transpose() / np.power(np.sum(np.power(query_feature, 2), axis=1), 0.5)
    query_feature = query_feature.transpose()
    print(f"query_feature: {query_feature.shape}")
    gallery_feature = gallery_feature.transpose() / np.power(np.sum(np.power(gallery_feature, 2), axis=1), 0.5)
    gallery_feature = gallery_feature.transpose()
    print(f"gallery_feature: {gallery_feature.shape}")

    st_distribution = scipy.io.loadmat(f"{working_dir}/model/{model_name}/spatial-temporal_features.mat")["distribution"]

    CMC = torch.IntTensor(len(gallery_label)).zero_()
    ap = 0.0
    for i in range(len(query_label)):
        ap_tmp, CMC_tmp = evaluate(query_feature[i], query_label[i], query_cam[i],
                                   query_frames[i], gallery_feature, gallery_label,
                                   gallery_cam, gallery_frames, st_distribution)
        if CMC_tmp[0] == -1:
            continue
        CMC += CMC_tmp
        ap += ap_tmp
        print(f"Processing label {i} / {len(query_label)}")

    CMC = CMC.float()
    CMC = CMC / len(query_label)  # average CMC
    print(f"rank-1: {CMC[0]} rank-5: {CMC[4]} rank-10: {CMC[9]} mAP: {ap/len(query_label)}")


if __name__ == "__main__":
    main()
