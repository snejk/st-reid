from __future__ import print_function, division

from urllib.request import urlopen
from zipfile import ZipFile
from pathlib import Path

zipurl = "http://188.138.127.15:81/Datasets/Market-1501-v15.09.15.zip"
working_dir = Path(".").absolute()
file_name = zipurl.split("/")[-1]
file_path = working_dir / file_name

if file_path.exists() is False:
    print("Downloading Market1501 dataset.")
    zipresp = urlopen(zipurl)
    with open(file_path, "wb") as f:
        f.write(zipresp.read())
        print(f"{file_name} downloaded successfully.")
else:
    print("File already downloaded.")

folder_path = f = Path(".").absolute() / "raw-dataset" / file_name.replace(".zip", "")
if folder_path.exists() is False:
    print(f"Extracting {file_name} to {str(folder_path).split('Market')[0]}")
    with ZipFile(file_path) as zf:
        zf.extractall(path=working_dir / "raw-dataset")
        print(f"{file_name} extracted successfully.")
else:
    print(f"{str(folder_path)} already exists.")
