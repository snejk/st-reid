# st-reID method

This repository is based on https://github.com/layumi/Person_reID_baseline_pytorch


## Requirements

- Python 3.8
- PyTorch
- torchvision
- Numpy
- SciPy


## Results

Rank-1 accuracy 97.47%

Rank-5 accuracy 99.26%

Rank-10 accuracy 99.40%

mAP score 87.70%


## Training your own model

Steps to reproduce the results:

1. Download the dataset

    `python download_market.py`

2. Prepare the dataset
    
    `python prepare.py`

3. Train the model

    `python train.py --PCB --train_all`

4. Extract visual features

    `python test.py --PCB`

5. Generate spatial-temporal features

    `python generate_st_model.py`

6. Evaluate both metrics

    `python evaluate.py`


## Extracted features

Already extracted visual and spatial-temporal features that can be used to evaluate the method can be downloaded [here](https://drive.google.com/drive/folders/15xyCvxGazf3eFZoZ3a2gkq9HHhHCrBDK?usp=sharing).

Put those two files into `./model/market1501/` and run `python evaluate.py`