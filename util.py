from __future__ import print_function, division

def parse_image_filename(images_path):
    camera_ids = []
    labels = []
    frames = []
    for path, label in images_path:
        # support for both windows and linux fs
        fname = path.split("\\")[-1] if "\\" in path else path.split("/")[-1]

        camera_id = int(fname.split("c")[1][0])
        frame = int(fname.split("_")[2][1:])

        # 0-indexing camera IDs
        camera_ids.append(camera_id - 1)
        labels.append(label)
        frames.append(frame)

    return camera_ids, labels, frames
