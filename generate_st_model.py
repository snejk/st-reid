from __future__ import print_function, division

import math
from pathlib import Path

import numpy as np
import scipy.io
from torchvision import datasets, transforms

from util import parse_image_filename


def spatial_temporal_distribution(camera_ids, labels, frames):
    class_count = len(list(set(labels)))
    cam_count = len(list(set(camera_ids)))
    eps = 1e-6
    interval = 100

    st_sum = np.zeros((class_count, cam_count))
    st_count = np.zeros((class_count, cam_count))

    for label, cam_id, frame in zip(labels, camera_ids, frames):
        st_sum[label][cam_id] = st_sum[label][cam_id] + frame
        st_count[label][cam_id] += 1
    st_avg = st_sum / (st_count + eps)

    distr = np.zeros((cam_count, cam_count, 10000))
    for i in range(class_count):
        # loop through all camera ID combinations
        for j in range(cam_count - 1):
            for k in range(j + 1, cam_count):
                # skip if the timestamp difference is between the same camera ID
                # skip if no timestamp is present
                if j == k or st_count[i][j] == 0 or st_count[i][k] == 0:
                    continue
                # label i appearing in the viewport of camera j at a specific timestamp
                ij_tstamp = st_avg[i][j]
                # label i appearing in the viewport of camera k at a specific timestamp
                ik_tstamp = st_avg[i][k]
                time_diff = int((ij_tstamp - ik_tstamp) / interval)

                if time_diff > 0:
                    distr[j][k][time_diff] += 1
                else:
                    distr[k][j][np.abs(time_diff)] += 1

    # apply the Parzen method to approximate and smoothen the PDF
    hist_sum = np.sum(distr, axis=-1)
    for i in range(cam_count):
        for j in range(cam_count):
            distr[i][j][:] = distr[i][j][:] / (hist_sum[i][j] + eps)
    for i in range(cam_count):
        for j in range(cam_count):
            distr[i][j][:] = gaussian_smoothing(distr[i][j][:], interval)

    return distr


def gaussian_smoothing(arr, std_deviation):
    hist_bin_count = len(arr)
    data_points = np.arange(hist_bin_count).reshape((hist_bin_count, 1))
    gaussian_values = gaussian_distribution(data_points, 0, std_deviation)
    matrix = np.zeros((hist_bin_count, hist_bin_count))

    # y ~ 0 if (x - mean) > 3 * std_deviation
    # used to speed up the calculation by rem0ving
    # values close to 0
    approximate_delta = 3 * std_deviation

    for i in range(hist_bin_count):
        k = 0
        for j in range(i, hist_bin_count):
            if k > approximate_delta:
                continue
            matrix[i][j] = gaussian_values[j - i]
            k += 1
    matrix = matrix + matrix.transpose()
    for i in range(hist_bin_count):
        matrix[i][i] = matrix[i][i] / 2

    return np.dot(matrix, arr)


def gaussian_distribution(x, mean, std_deviation):
    fraction = 1.0 / (std_deviation * math.sqrt(2 * math.pi))
    exponent = np.exp(-0.5 * (np.power(x - mean, 2) / np.power(std_deviation, 2)))
    return fraction * exponent


def main():
    model_name = "market1501"
    working_dir = Path(".").absolute()
    data_dir = working_dir / "dataset" / "Market-1501_prepared"

    transform_train_list = [
        transforms.Resize(144, interpolation=3),
        transforms.RandomCrop((256, 128)),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])  # values taken from https://pytorch.org/vision/stable/models.html
    ]

    image_datasets = {
        "train_all": datasets.ImageFolder(data_dir / "train_all", transform_train_list)
    }

    train_path = image_datasets['train_all'].imgs
    train_cam, train_label, train_frames = parse_image_filename(train_path)

    distribution = spatial_temporal_distribution(train_cam, train_label, train_frames)

    features = {
        'distribution': distribution
    }
    scipy.io.savemat(f"{working_dir}/model/{model_name}/spatial-temporal_features.mat", features)


if __name__ == "__main__":
    main()
