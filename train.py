from __future__ import print_function, division

import argparse
import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import numpy as np
from torchvision import datasets, transforms
import time
import os
from model import ft_net, ft_net_dense, PCB
from random_erasing import RandomErasing
from pathlib import Path

def main(opt):

    working_dir = Path(".").absolute()
    data_dir = opt.data_dir
    name = opt.name
    gpu_ids = [int(gpu_id) for gpu_id in opt.gpu_ids.split(",")]

    # set gpu ids
    if len(gpu_ids) > 0:
        torch.cuda.set_device(gpu_ids[0])

    if not os.path.exists("./model/"):
        os.makedirs("./model/")

    # Load Data
    transform_train_list = [
            transforms.Resize((288,144), interpolation=3),
            transforms.RandomCrop((256,128)),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]

    transform_val_list = [
            transforms.Resize(size=(256,128), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]

    if opt.PCB:
        transform_train_list = [
            transforms.Resize((384,192), interpolation=3),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]
        transform_val_list = [
            transforms.Resize(size=(384,192), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
            ]

    if opt.erasing_p > 0:
        transform_train_list = transform_train_list +  [RandomErasing(probability = opt.erasing_p, mean=[0.0, 0.0, 0.0])]

    if opt.color_jitter:
        transform_train_list = [transforms.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0)] + transform_train_list

    print(transform_train_list)
    data_transforms = {
        'train': transforms.Compose( transform_train_list ),
        'val': transforms.Compose(transform_val_list),
    }

    train_all = ''
    if opt.train_all:
        train_all = '_all'

    image_datasets = {}
    image_datasets['train'] = datasets.ImageFolder(os.path.join(data_dir, 'train' + train_all),
                                                   data_transforms['train'])
    image_datasets['val'] = datasets.ImageFolder(os.path.join(data_dir, 'val'),
                                                 data_transforms['val'])

    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=opt.batchsize,
                                                  shuffle=True, num_workers=opt.num_workers)
                   for x in ['train', 'val']}
    dataset_sizes = {x: len(image_datasets[x]) for x in ['train', 'val']}
    class_names = image_datasets['train'].classes

    use_gpu = torch.cuda.is_available()

    inputs, classes = next(iter(dataloaders['train']))

    # Training the model
    y_loss = {}
    y_loss['train'] = []
    y_loss['val'] = []
    y_err = {}
    y_err['train'] = []
    y_err['val'] = []

    def train_model(model, criterion, optimizer, scheduler, num_epochs=25):
        since = time.time()

        for epoch in range(num_epochs):
            print('Epoch {}/{}'.format(epoch, num_epochs - 1))
            print('-' * 10)

            # Each epoch has a training and validation phase
            for phase in ['train', 'val']:
                if phase == 'train':
                    optimizer.step()
                    scheduler.step()
                    model.train(True)  # Set model to training mode
                else:
                    model.train(False)  # Set model to evaluate mode

                running_loss = 0.0
                running_corrects = 0
                # Iterate over data.
                for data in dataloaders[phase]:
                    # get the inputs
                    inputs, labels = data
                    # wrap them in Variable
                    if use_gpu:
                        inputs = Variable(inputs.cuda())
                        labels = Variable(labels.cuda())
                    else:
                        inputs, labels = Variable(inputs), Variable(labels)

                    # zero the parameter gradients
                    optimizer.zero_grad()

                    # forward
                    outputs = model(inputs)
                    if not opt.PCB:
                        _, preds = torch.max(outputs.data, 1)
                        loss = criterion(outputs, labels)
                    else:
                        part = {}
                        sm = nn.Softmax(dim=1)
                        num_part = 6
                        for i in range(num_part):
                            part[i] = outputs[i]

                        score = sm(part[0]) + sm(part[1]) +sm(part[2]) + sm(part[3]) +sm(part[4]) +sm(part[5])
                        _, preds = torch.max(score.data, 1)

                        loss = criterion(part[0], labels)
                        for i in range(num_part-1):
                            loss += criterion(part[i+1], labels)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                    # statistics
                    running_loss += loss.item()
                    running_corrects += torch.sum(preds == labels.data)

                epoch_loss = running_loss / dataset_sizes[phase]
                epoch_acc = (running_corrects.item()) / dataset_sizes[phase]

                print('{} Loss: {:.4f} Acc: {:.4f}'.format(
                    phase, epoch_loss, epoch_acc))

                y_loss[phase].append(epoch_loss)
                y_err[phase].append(1.0-epoch_acc)
                # deep copy the model
                if phase == 'val':
                    last_model_wts = model.state_dict()
                    if epoch%10 == 9:
                        save_network(model, epoch)

            print()

        time_elapsed = time.time() - since
        print(f"Training complete in {(time_elapsed // 60):.0f}m {(time_elapsed % 60):.0f}s")

        # load best model weights
        model.load_state_dict(last_model_wts)
        save_network(model, 'last')
        return model

    # Save model
    def save_network(network, epoch_label):
        save_filename = f"net_{epoch_label}.pth"
        save_path = os.path.join('./model', name, save_filename)
        torch.save(network.cpu().state_dict(), save_path)
        if torch.cuda.is_available:
            network.cuda(gpu_ids[0])

    # Finetuning the convnet
    if opt.use_dense:
        model = ft_net_dense(len(class_names))
    else:
        model = ft_net(len(class_names))

    if opt.PCB:
        model = PCB(len(class_names))

    print(model)

    if use_gpu:
        model = model.cuda()

    criterion = nn.CrossEntropyLoss()

    if not opt.PCB:
        ignored_params = list(map(id, model.model.fc.parameters() )) + list(map(id, model.classifier.parameters() ))
        base_params = filter(lambda p: id(p) not in ignored_params, model.parameters())
        optimizer_ft = optim.SGD([
                {'params': base_params, 'lr': 0.01},
                {'params': model.model.fc.parameters(), 'lr': 0.1},
                {'params': model.classifier.parameters(), 'lr': 0.1}
            ], weight_decay=5e-4, momentum=0.9, nesterov=True)
    else:
        ignored_params = list(map(id, model.model.fc.parameters() ))
        ignored_params += (
            list(map(id, model.classifier0.parameters() ))
            + list(map(id, model.classifier1.parameters() ))
            + list(map(id, model.classifier2.parameters() ))
            + list(map(id, model.classifier3.parameters() ))
            + list(map(id, model.classifier4.parameters() ))
            + list(map(id, model.classifier5.parameters() ))
        )
        base_params = filter(lambda p: id(p) not in ignored_params, model.parameters())
        optimizer_ft = optim.SGD([
                {'params': base_params, 'lr': 0.01},
                {'params': model.model.fc.parameters(), 'lr': 0.1},
                {'params': model.classifier0.parameters(), 'lr': 0.1},
                {'params': model.classifier1.parameters(), 'lr': 0.1},
                {'params': model.classifier2.parameters(), 'lr': 0.1},
                {'params': model.classifier3.parameters(), 'lr': 0.1},
                {'params': model.classifier4.parameters(), 'lr': 0.1},
                {'params': model.classifier5.parameters(), 'lr': 0.1},
            ], weight_decay=5e-4, momentum=0.9, nesterov=True)

    # Decay LR by a factor of 0.1 every 40 epochs
    exp_lr_scheduler = lr_scheduler.StepLR(optimizer_ft, step_size=40, gamma=0.1)

    # Train and evaluate
    dir_name = os.path.join('./model', name)
    if not os.path.isdir(dir_name):
        os.mkdir(dir_name)

    model = train_model(model, criterion, optimizer_ft, exp_lr_scheduler, num_epochs=60)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Training')
    parser.add_argument('--gpu_ids', default='0', type=str,help='gpu_ids: e.g. 0  0,1,2  0,2')
    parser.add_argument('--name', default='market1501', type=str, help='output model name')
    parser.add_argument('--data_dir', default="dataset/Market-1501_prepared",type=str, help='training dir path')
    parser.add_argument('--train_all', action='store_true', help='use all training data' )
    parser.add_argument('--color_jitter', action='store_true', help='use color jitter in training' )
    parser.add_argument('--batchsize', default=4, type=int, help='batchsize')
    parser.add_argument('--erasing_p', default=0.5, type=float, help='Random Erasing probability, in [0,1]')
    parser.add_argument('--use_dense', action='store_true', help='use densenet121' )
    parser.add_argument('--PCB', action='store_true', help='use PCB+ResNet50' )
    parser.add_argument('--num_workers', default=8, action='store_true', help='number of processes')
    options = parser.parse_args()
    main(options)