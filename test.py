from __future__ import print_function, division

import argparse
from pathlib import Path
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
from torchvision import datasets, transforms
import os
import scipy.io
from model import ft_net, ft_net_dense, PCB, PCB_test
from util import parse_image_filename

def main(opt):

    opt.nclasses = 751
    name = opt.name
    working_dir = Path(".").absolute()
    test_dir = opt.test_dir

    gpu_ids = [int(gpu_id) for gpu_id in opt.gpu_ids.split(",")]
    if len(gpu_ids) > 0:
        torch.cuda.set_device(gpu_ids[0])

    # Load Data
    data_transforms = transforms.Compose([
            transforms.Resize((288,144), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    if opt.PCB:
        data_transforms = transforms.Compose([
            transforms.Resize((384,192), interpolation=3),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225]) 
        ])

    data_dir = test_dir
    image_datasets = {
        x: datasets.ImageFolder(os.path.join(data_dir, x) ,data_transforms)
        for x in ['gallery', 'query']
    }

    dataloaders = {
        x: torch.utils.data.DataLoader(image_datasets[x], batch_size=opt.batchsize,
                                       shuffle=False, num_workers=opt.num_workers)
                                       for x in ['gallery', 'query']
    }

    class_names = image_datasets['query'].classes
    use_gpu = torch.cuda.is_available()

    # Load model
    def load_network(network):
        save_path = Path(".").absolute() / f"model/{name}/net_{opt.which_epoch}.pth"
        network.load_state_dict(torch.load(save_path))
        return network


    def fliplr(img):
        '''flip image horizontally'''
        # N x C x H x W
        inv_idx = torch.arange(img.size(3)-1,-1,-1).long()
        img_flip = img.index_select(3,inv_idx)
        return img_flip


    def extract_feature(model,dataloaders):
        features = torch.FloatTensor()
        count = 0
        for data in dataloaders:
            img, label = data
            n, c, h, w = img.size()
            count += n
            print(f"{count // n} / {len(dataloaders)}")
            if opt.use_dense:
                ff = torch.FloatTensor(n, 1024).zero_()
            else:
                ff = torch.FloatTensor(n, 2048).zero_()
            if opt.PCB:
                ff = torch.FloatTensor(n, 2048, 6).zero_()
            for i in range(2):
                if(i==1):
                    img = fliplr(img)
                input_img = Variable(img.cuda())
                outputs = model(input_img) 
                f = outputs.data.cpu()
                ff = ff+f
            # normalize features
            if opt.PCB:
                # feature size (n, 2048, 4)
                fnorm = torch.norm(ff, p=2, dim=1, keepdim=True)
                ff = ff.div(fnorm.expand_as(ff))
                ff = ff.view(ff.size(0), -1)
            else:
                fnorm = torch.norm(ff, p=2, dim=1, keepdim=True)
                ff = ff.div(fnorm.expand_as(ff))

            features = torch.cat((features,ff), 0)
        return features


    gallery_path = image_datasets['gallery'].imgs
    query_path = image_datasets['query'].imgs

    gallery_cam, gallery_label, gallery_frames = parse_image_filename(gallery_path)
    query_cam, query_label, query_frames = parse_image_filename(query_path)

    # Load collected data trained model
    print('-------test-----------')
    if opt.use_dense:
        model_structure = ft_net_dense(opt.nclasses)
    else:
        model_structure = ft_net(opt.nclasses)

    if opt.PCB:
        model_structure = PCB(opt.nclasses)

    model = load_network(model_structure)

    # Remove the final fully connected layer and classifier layer
    if not opt.PCB:
        model.model.fc = nn.Sequential()
        model.classifier = nn.Sequential()
    else:
        model = PCB_test(model)

    # Change to test mode
    model = model.eval()
    if use_gpu:
        model = model.cuda()

    # Extract features
    gallery_feature = extract_feature(model,dataloaders['gallery'])
    query_feature = extract_feature(model,dataloaders['query'])

    # Save to a file for evaluation
    result = {
        'gallery_f': gallery_feature.numpy(),
        'gallery_label': gallery_label,
        'gallery_cam': gallery_cam,
        'gallery_frames': gallery_frames,
        'query_f': query_feature.numpy(),
        'query_label': query_label,
        'query_cam': query_cam,
        'query_frames': query_frames
    }
    scipy.io.savemat(f"{working_dir}/model/{name}/visual_features.mat", result)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Test')
    parser.add_argument('--gpu_ids', default='0', type=str,help='gpu_ids: e.g. 0  0,1,2  0,2')
    parser.add_argument('--which_epoch', default='last', type=str, help='0,1,2,3...or last')
    parser.add_argument('--test_dir', default="dataset/Market-1501_prepared", type=str, help='./test_data')
    parser.add_argument('--name', default='market1501', type=str, help='save model path')
    parser.add_argument('--batchsize', default=4, type=int, help='batchsize')
    parser.add_argument('--use_dense', action='store_true', help='use densenet121')
    parser.add_argument('--PCB', action='store_true', help='use PCB')
    parser.add_argument('--num_workers', default=8, action='store_true', help='number of processes')
    options = parser.parse_args()
    main(options)
